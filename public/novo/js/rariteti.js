$("#hr").click(function() {
    localStorage.setItem('jezik', 'hr');
    promijeni_jezik()
})

$("#eng").click(function() {
    localStorage.setItem('jezik', 'eng');
    promijeni_jezik()
})

$("#it").click(function() {
    localStorage.setItem('jezik', 'it');
    promijeni_jezik()
})

$('.parallax-window').parallax({
    imageSrc: 'slike/police.jpg'
});

$('.parallax-window').parallax({

});

$(document).ready(function() {
    promijeni_jezik()

});

function promijeni_jezik() {
     let searchParams = new URLSearchParams(window.location.search)
    searchParams.has('j') // true
    let param = searchParams.get('j')

    if (param == "hr") {
        localStorage.setItem('jezik', 'hr')
    } else if (param == "it") {
        localStorage.setItem('jezik', 'it')
    } else if (localStorage.getItem("jezik") == null) {
        localStorage.setItem('jezik', 'hr')
    }
    if (localStorage.getItem("jezik") == "hr") {

        $("#hr").css({
            "font-weight": "bold",
            "opacity": "1"
        })
        $("#eng").css({
            "font-weight": "normal"
        })


        $("#it").css({
            "font-weight": "normal",
            "opacity": "0.5"
        })

        $(".naznas").text("Naslovnica")
        $(".nazdjela").text("Djela")
        $(".naztis").text("Tiskarski rariteti")
        $(".naz16").text("Rariteti iz 16. stoljeća")
        $(".nazzbi").text("Stancoviciana")
        $(".opisslike").text("Petar Stanković, Autoportret, 1792.")
        $(".nazsta").text("Petar Stanković")
        $(".str").text("str.")
        $(".list").text("list")
        $(".ilu").text("ilustr.")
        $(".muz").text("Muzej Grada Rovinja – Rovigno")
  $(".podtis").text("Podatak o izdavanju preuzet iz kolofona.")


        $(".presavtab").text("presavijena lista s tablama")
        $(".presav").text("presavijena lista")




        $(".naslov").text("TISKARSKI RARITETI")
        $(".izmijena").html('<p> <strong>ALDINE</strong></p><p> Knjige koje je tiskao znameniti mletački tiskar i humanist Aldo Manuzio (1449-1515) i njegovi nasljednici.</p><p> Manuzio je izdavao djela grčkih i latinskih klasika; prvi je upotrijebio kurzivna slova (“italska“ ili “aldine“) te uveo format osminu. Nakon smrti A. Manuzia posao je uspješno vodio njegov sin Paolo.</p><p> <strong>PLANTINE</strong></p><p> Knjige najslavnijeg europskog izdavača druge polovice 16. stoljeća - Christopha Plantina (1520-1589). On je tiskao oko 2000 raskošno opremljenih i bogato ilustriranih knjiga. Posjedovao je najbolje opremljenu tiskaru u tadašnjoj Europi. Nakon njegove smrti tiskaru je preuzeo Plantinov zet Moretus s obitelji.</p><p> <strong>ELZEVIERI</strong></p><p> Nizozemska obitelj Elzevier imala je tijekom 17. stoljeća najzanačajniju tiskarsko-izdavačku tvrtku u Europi. Izdali su velik broj jeftinih, ali sadržajno kvalitetnih izdanja manjeg formata. Među bibliofilima njihove su knjige poznate kao “elzevieri“, a čuvene su po kvalitetnom tisku te pouzdanom tekstu.</p><p> <strong>OBITELJ GIUNTA</strong></p><p> Imala je u Veneciji početkom 16. stoljeća znamenitu tiskaru. Izdavali su serije klasičnih pisaca izuzetne ljepote i korektnosti tiska.</p><p> <strong>OBITELJ DIDOT</strong></p><p> Tiskarska i izdavačka obitelj Didot započela je s radom u drugoj polovici 18. stoljeća u Francuskoj. Poznati su po izdavanju niza jeftinih i kvalitetnih djela klasičnih i novijih pisaca, tiskanih posebnim vrstama slova.</p>')

    } else if (localStorage.getItem("jezik") == "eng") {

        $("#hr").css({
            "font-weight": "normal"
        })
        $("#eng").css({
            "font-weight": "bold"
        })

        $("#it").css({
            "font-weight": "normal"
        })

        $(".izmijena").html('<p>PETAR STANKOVIĆ – PIETRO STANCOVICH (Barbana, 24 febbraio 1771 - Barbana, 12 settembre 1852) canonico e storiografo, iniziò il suo lungo percorso scolastico e di studi a Rovigno e grazie alla grande dedizione per l’attività intellettuale e al desiderio di imparare, ebbe durante la sua vita enormi successi nel campo delle scienze. Il suo costante e attivo impegno nello studio lo arricchiva intellettualmente, portandolo anche ad essere membro di numerose accademie.</p><p> Analizzando l\'opera omnia di Stancovich, ovvero i ventitre scritti pubblicati, i suoi manoscritti e i titoli dei libri da lui collezionati con passione per la sua biblioteca, notiamo la varietà degli interessi personali di questo personaggio. I volumi da lui donati alla città di Rovigno illustrano le tendenze della vita sociale, economica e culturale dell\'epoca e i cambiamenti del territorio. Stancovich ci ha pertanto offerto un autentico tesoro per le ricerche e lo studio del patrimonio librario che non ha solo un mero valore storico e pratico, ma anche un significato artistico quale parte dell\'identità civica e nazionale. L’uomo Pietro Stancovich ci ricorda che non esiste vita proficua senza abnegazione.</p>')

    } else {
        $("#hr").css({
            "font-weight": "normal",
            "opacity": "0.5"
        })
        $("#eng").css({
            "font-weight": "normal"
        })

        $("#it").css({
            "font-weight": "bold",
            "opacity": "1"
        })
        $(".naznas").text("Pagina iniziale")
        $(".nazdjela").text("Opere")
        $(".naztis").text("Rarità di stampa")
        $(".naz16").text("Rarità del secolo XVI")
        $(".nazzbi").text("Stancoviciana")
        $(".opisslike").text("Petar Stanković, autoritratto, 1792.")
        $(".nazsta").text("Pietro Stancovich")
        $(".str").text("p.")
        $(".list").text("foglio")
        $(".presavtab").text("fogli ripiegati con tavole,")
        $(".presav").text("fogli ripiegati")
        $(".ilu").text("ill.")
  $(".muz").text("Museo della Città di Rovinj – Rovigno")
  $(".podtis").text("Dati della edizione da colofon.")

        $(".naslov").text("RARITÀ DI STAMPA")
        $(".izmijena").html('<p> <strong>ALDINE</strong></p><p> Libri stampati dal famoso editore e umanista veneziano Aldo Manuzio (1449-1515) e dai suoi discendenti Manuzio pubblicava opere dei classici greci e latini; è stato il primo ad usare i caratteri corsivi (“italici“ o “aldini“) ed ha introdotto il formato dell\'ottavo.</p><p> Alla sua morte, l\'opera di A. Manuzio è stata proseguita con successo da suo figlio Paolo.</p><p><strong>PLANTINE</strong></p><p> Libri del più celebre editore europeo della seconda metà del secolo XVI, Cristoph Plantin (1520-1589). Ha stampato circa 2000 libri redatti con lusso e ricchi di illustrazioni. Possedeva la stamperia è stata portata avanti da suo genero Moretus e dalla famiglia.</p><p> <strong>ELZEVIERI</strong></p><p> La famiglia olandese degli Elzevier aveva, nel corso del secolo XVII, la più importante ditta editoriale e stamperia d\'Europa. Pubblicavano molti libri a basso costo, di formato minore, di ottimo contenuto. Fra i bibliofili i suoi libri sono noti con il nome di “elzevieri“ e sono conosciuti per la buona stampa e il testo affida-bile.</p><p> <strong>FAMIGLIA GIUNTA</strong></p><p> All\'inizio del secolo XVI teneva a Venezia una famosa stamperia. Hanno stampato varie serie di scrittori classici, eccezionali per la bellezza e la correttezza della stampa.</p><p> <strong>FAMIGLIA DIDOT</strong></p><p> La famiglia Didot ha iniziato il lavoro di stampa e di edizione nella seconda metà del secolo XVIII in Francia. Sono noti per l\'edizione di una serie di opere classiche e di scrittori più recenti, stampate con tipi particolari di carattere, edizioni poco costose e di alta qualità.</p>')
    }
}
/*
$(document).scroll(function() {
    var st = $(this).scrollTop();
    $("#header").css({
        "background-position-y": (-st / 20)
    })
    $("#headerc").css({
        "top": (-st / 5),
        "bottom": (st / 5)
    })
});

window.onscroll = function() {
    myFunction()
};

var navbar = document.getElementById("navbar");
var sidebar = document.getElementById("sidenav");
var sticky = navbar.offsetTop;

function myFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
        sidebar.classList.add("sticky")
    } else {
        navbar.classList.remove("sticky");
        sidebar.classList.remove("sticky");

    }
}
*/
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
    dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var dropdownContent = this.nextElementSibling;
        if (dropdownContent.style.display === "block") {
            dropdownContent.style.display = "none";
        } else {
            dropdownContent.style.display = "block";
        }
    });
}




$(document).mouseup(function(e) {
    var container = $(".slikica");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $("#glass").hide();
    }
});