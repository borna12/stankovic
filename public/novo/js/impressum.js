

$("#it").click(function() {
    localStorage.setItem('jezik', 'it');
    promijeni_jezik()
})

$('.parallax-window').parallax({
    imageSrc: 'slike/police.jpg'
});

$('.parallax-window').parallax({

});


function promijeni_jezik() {
    $(".naslov").text("IMPRESSUM")

     let searchParams = new URLSearchParams(window.location.search)
    searchParams.has('j') // true
    let param = searchParams.get('j')

    if (param == "hr") {
        localStorage.setItem('jezik', 'hr')
    } else if (param == "it") {
        localStorage.setItem('jezik', 'it')
    } else if (localStorage.getItem("jezik") == null) {
        localStorage.setItem('jezik', 'hr')
    }
    if (localStorage.getItem("jezik") == "hr") {

         $("#hr").css({
            "font-weight": "bold",
             "opacity": "1"
        })
        $("#eng").css({
            "font-weight": "normal"
        })

       
        $("#it").css({
            "font-weight": "normal",
             "opacity": "0.5"
        }) 
        

        $(".naznas").text("Naslovnica")
        $(".nazdjela").text("Djela")
        $(".naztis").text("Tiskarski rariteti")
        $(".naz16").text("Rariteti iz 16. stoljeća")
        $(".nazzbi").text("Stancoviciana")
        $(".nazsta").text("Petar Stanković")



        $(".content").html('<p><strong>Muzej Grada Rovinja – Rovigno – Museo della Città di Rovinj – Rovigno</strong></p><p> <img src="slike/grb.png" style="max-width:100px;height:auto"></p><p> <strong>Spomenička knjižnica Stancoviciana</strong></p><p> <strong> </strong></p><p> <strong> </strong></p><p> <strong>NAKLADNIK</strong>:</p><p> <ul><li class="smece">Muzej Grada Rovinja – Rovigno – Museo della Città di Rovinj – Rovigno</li></ul></p><p> <strong>ZA NAKLADNIKA</strong> :</p><p> <ul><li class="smece">Marija Smolica</ul></li></p><p> <strong>AUTORICA IZLOŽBE</strong>:</p><p> <ul><li class="smece">Andrea Milotić</ul></li></p><strong>SURADNICA</strong>:</p><p> <ul><li class="smece">Marija Smolica</ul></li></p><p> <strong> </strong></p><p> <strong>DIZAJN I PROGRAMIRANJE:</strong></p><p> <ul><li class="smece">Link2 d.o.o.</ul></li></p><p> <strong> </strong></p><p> <strong>DIGITALIZACIJA GRAĐE:</strong></p><p> <ul><li class="smece">Andrea Milotić</li></ul></p><p> <strong> </strong></p><p> <strong>PRIJEVOD NA TALIJANSKI JEZIK:</strong></p><p> <ul><li class="smece">Atinianum d.o.o. Vodnjan-Dignano</ul></li></p><p> <strong> Primarna teorijska podloga pri realizaciji virtualne izložbe je: </strong></p><p> <ul><li class="smece">Stancoviciana: katalog izložbe: Zavičajni muzej Rovinj / [tekst kataloga Bruno Dobrić ; biografija Petra Stankovića Miroslav Bertoša; prijevodi Eufemia Papić, Silvana Kos]. Rovinj: Zavičajni muzej Rovinj, 1992.</li></ul></p><p> <strong> Virtualna izložba financirana je sredstvima Grada Rovinja ̶ Rovigno </strong></p>')

    } else if (localStorage.getItem("jezik") == "eng") {

        $("#hr").css({
            "font-weight": "normal"
        })
        $("#eng").css({
            "font-weight": "bold"
        })

        $("#it").css({
            "font-weight": "normal"
        })


        $(".content").html('<p> <strong>Museo della Città di Rovinj-Rovigno</strong></p><p> <strong> </strong></p><p> <strong>Biblioteca memoriale Stancoviciana</strong></p><p> <strong> </strong></p><p> <strong> </strong></p><p> <strong>EDITORE:</strong></p><p> <ul><li class="smece">Museo della Città di Rovinj-Rovigno</li></ul></p><p> <strong>PER L\'EDITORE:</strong></p><p><ul><li class="smece"> Marija Smolica</ul></li></p><p> <strong>AUTRICI DELLA MOSTRA:</strong></p><p> Andrea Milotić, Marija Smolica</p><p> <strong> </strong></p><p> <strong>DESIGN E PROGRAMMAZIONE:</strong></p><p> Link2 d.o.o.</p><p> <strong> </strong></p><p> <strong>DIGITALIZZAZIONE DEL MATERIALE:</strong></p><p> Andrea Milotić</p><p> <strong> </strong></p><p> <strong>TRADUZIONE ITALIANA:</strong></p><p> Atinianum d.o.o. Vodnjan-Dignano</p><p> <strong> </strong></p><p> <strong> Fondamento teorico primario della realizzazione della mostra virtuale: </strong></p><p> Stancoviciana : catalogo della mostra: Museo civico di Rovigno / [testo del catalogo; biografia Petra Stankovića Miroslav Bertoša ; traduzioni Eufemia Papić, Silvana Kos]. Rovinj: Museo civico di Rovigno, 1992.</p><p> <strong> La mostra virtuale è stata finanziata con i mezzi della Città di Rovinj </strong> <strong>-</strong> <strong> Rovigno</strong></p>')

    } else {
        $("#hr").css({
            "font-weight": "normal",
            "opacity": "0.5"
        })
        $("#eng").css({
            "font-weight": "normal"
        })

         $("#it").css({
            "font-weight": "bold",
            "opacity": "1"
        })
        $(".naznas").text("Pagina iniziale")
        $(".nazdjela").text("Opere")
        $(".naztis").text("Rarità di stampa")
        $(".naz16").text("Rarità del secolo XVI")
        $(".nazzbi").text("Stancoviciana")
        $(".nazsta").text("Pietro Stancovich")

        $(".content").html('<p><strong>Muzej Grada Rovinja – Rovigno – Museo della Città di Rovinj – Rovigno</strong></p><p> <img src="slike/grb.png" style="max-width:100px;height:auto"></p><p> <strong>Biblioteca memoriale Stancoviciana</strong></p><p> <strong> </strong></p><p> <strong> </strong></p><p> <strong>EDITORE:</strong></p><p> <ul><li class="smece">Muzej Grada Rovinja – Rovigno – Museo della Città di Rovinj – Rovigno</li></ul></p><p> <strong>PER L\'EDITORE:</strong></p><p><ul><li class="smece"> Marija Smolica</ul></li></p><p> <strong>AUTORE DELLA MOSTRA:</strong></p><p> <ul><li class="smece">Andrea Milotić</ul></li></p><p> <strong>COLLABORATORE:</strong></p><p> <ul><li class="smece">Marija Smolica</ul></li></p><p> <strong> </strong></p><p> <strong>DESIGN E PROGRAMMAZIONE:</strong></p><p> <ul><li class="smece">Link2 d.o.o.</li></ul></p><p> <strong> </strong></p><p> <strong>DIGITALIZZAZIONE DEL MATERIALE:</strong></p><p> <ul><li class="smece">Andrea Milotić</li></ul></p><p> <strong> </strong></p><p> <strong>TRADUZIONE ITALIANA:</strong></p><p> <ul><li class="smece">Atinianum d.o.o. Vodnjan-Dignano</li></ul></p><p> <strong> </strong></p><p> <strong> Fondamento teorico primario della realizzazione della mostra virtuale: </strong></p><p> <ul><li class="smece">Stancoviciana : catalogo della mostra: Museo civico di Rovigno / [testo del catalogo; biografia Petra Stankovića Miroslav Bertoša ; traduzioni Eufemia Papić, Silvana Kos]. Rovinj: Museo civico di Rovigno, 1992.</li></ul></p><p> <strong> La mostra virtuale è stata finanziata con i mezzi della Città di Rovinj ̶  Rovigno</p>')
    }
}
/*
$(document).scroll(function() {
    var st = $(this).scrollTop();
    $("#header").css({
        "background-position-y": (-st / 20)
    })
    $("#headerc").css({
        "top": (-st / 5),
        "bottom": (st / 5)
    })
});

window.onscroll = function() {
    myFunction()
};

var navbar = document.getElementById("navbar");
var sidebar = document.getElementById("sidenav");
var sticky = navbar.offsetTop;

function myFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
        sidebar.classList.add("sticky")
    } else {
        navbar.classList.remove("sticky");
        sidebar.classList.remove("sticky");

    }
}
*/
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
    dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var dropdownContent = this.nextElementSibling;
        if (dropdownContent.style.display === "block") {
            dropdownContent.style.display = "none";
        } else {
            dropdownContent.style.display = "block";
        }
    });
}