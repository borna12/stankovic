$("#hr").click(function() {
    localStorage.setItem('jezik', 'hr');
    promijeni_jezik()
})

$("#eng").click(function() {
    localStorage.setItem('jezik', 'eng');
    promijeni_jezik()
})

$("#it").click(function() {
    localStorage.setItem('jezik', 'it');
    promijeni_jezik()
})

$('.parallax-window').parallax({
    imageSrc: 'slike/police.jpg'
});

$('.parallax-window').parallax({

});

$(document).ready(function() {
    promijeni_jezik()

});

function promijeni_jezik() {
     let searchParams = new URLSearchParams(window.location.search)
    searchParams.has('j') // true
    let param = searchParams.get('j')

    if (param == "hr") {
        localStorage.setItem('jezik', 'hr')
    } else if (param == "it") {
        localStorage.setItem('jezik', 'it')
    } else if (localStorage.getItem("jezik") == null) {
        localStorage.setItem('jezik', 'hr')
    }
    if (localStorage.getItem("jezik") == "hr") {

        $("#hr").css({
            "font-weight": "bold",
            "opacity": "1"
        })
        $("#eng").css({
            "font-weight": "normal"
        })


        $("#it").css({
            "font-weight": "normal",
            "opacity": "0.5"
        })

        $(".naznas").text("Naslovnica")
        $(".nazdjela").text("Djela")
        $(".naztis").text("Tiskarski rariteti")
        $(".naz16").text("Rariteti iz 16. stoljeća")
        $(".nazzbi").text("Stancoviciana")
        $(".opisslike").text("Petar Stanković, Autoportret, 1792.")
        $(".nazsta").text("Petar Stanković")
        $(".str").text("str.")
        $(".list").text("list")
        $(".listova").text("listova")
        $(".ilu").text("ilustr.")
        $(".muz").text("Muzej Grada Rovinja – Rovigno")

        $(".naslov").text("HISTRICA")
        $(".podtis").text("Podaci o tiskaru preuzeti iz kolofona. - Kožni uvez. - Na nasl. str. pečat: Pietro Stancovich Canonico. - Drugi pečat nepoznatog vlasnika s inicijalima \"AB\".")
        $(".izmijena").html('<p> Stanković je sustavno nabavljao knjige istarskih autora, npr. G. Muzia, G. R. Carlia, P. Predonzania, G. Albertinia, te knjige u kojima se spominje Istra. Iz istih ovih knjiga, kao i iz mnogih drugih koje je pronašao u velikim talijanskom knjižnicama, crpio je građu za svoje opsežno bio-bibliografsko djelo “Biografia degli uomini distinti dell\'Istria“ (Životopisi znamenitih ljudi iz Istre), objavljeno u Trstu 1828.-1829. godine. U njemu uz rimske i talijanske istaknute ličnosti navodi i nekoliko istarskih Hrvata, npr. Franju Glavinića.</p><p> Andrea Divo / Divus iz Kopra, u 16. stoljeću objavio je latinske prijevode Homera, Teokrita i Aristofana.</p><p> Giovanni Tatio (Tacco), također iz Kopra, djelovao je u 16. stoljeću.</p><p> Giovanni Antonio Pantera (16. st.), rođen u Novigradu, služio je kao kanonik u porečkoj katedrali.</p><p> Giacomo Filippo Tomasini (1595-1655), biskup u Novigradu.</p><p> Franjo Glavinić (Kanfanar, 1585; Trsat, 1652) pisac, franjevac. Objavio je djela na hrvatskom, talijanskom i latinskom jeziku.</p>')

    } else if (localStorage.getItem("jezik") == "eng") {

        $("#hr").css({
            "font-weight": "normal"
        })
        $("#eng").css({
            "font-weight": "bold"
        })

        $("#it").css({
            "font-weight": "normal"
        })

        $(".izmijena").html('<p>PETAR STANKOVIĆ – PIETRO STANCOVICH (Barbana, 24 febbraio 1771 - Barbana, 12 settembre 1852) canonico e storiografo, iniziò il suo lungo percorso scolastico e di studi a Rovigno e grazie alla grande dedizione per l’attività intellettuale e al desiderio di imparare, ebbe durante la sua vita enormi successi nel campo delle scienze. Il suo costante e attivo impegno nello studio lo arricchiva intellettualmente, portandolo anche ad essere membro di numerose accademie.</p><p> Analizzando l\'opera omnia di Stancovich, ovvero i ventitre scritti pubblicati, i suoi manoscritti e i titoli dei libri da lui collezionati con passione per la sua biblioteca, notiamo la varietà degli interessi personali di questo personaggio. I volumi da lui donati alla città di Rovigno illustrano le tendenze della vita sociale, economica e culturale dell\'epoca e i cambiamenti del territorio. Stancovich ci ha pertanto offerto un autentico tesoro per le ricerche e lo studio del patrimonio librario che non ha solo un mero valore storico e pratico, ma anche un significato artistico quale parte dell\'identità civica e nazionale. L’uomo Pietro Stancovich ci ricorda che non esiste vita proficua senza abnegazione.</p>')

    } else {
        $("#hr").css({
            "font-weight": "normal",
            "opacity": "0.5"
        })
        $("#eng").css({
            "font-weight": "normal"
        })

        $("#it").css({
            "font-weight": "bold",
            "opacity": "1"
        })
        $(".naznas").text("Pagina iniziale")
        $(".nazdjela").text("Opere")
        $(".naztis").text("Rarità di stampa")
        $(".naz16").text("Rarità del secolo XVI")
        $(".nazzbi").text("Stancoviciana")
        $(".opisslike").text("Petar Stanković, autoritratto, 1792.")
        $(".nazsta").text("Pietro Stancovich")
        $(".str").text("p.")
        $(".list").text("foglio")
        $(".listova").text("fogli")
        $(".ilu").text("ill.")
        $(".muz").text("Museo della Città di Rovinj – Rovigno")
 $(".podtis").text("Dati della stampa da colofon. – Rilegatura in pelle. – Sulla copertina è  un sigillo: Pietro Stancovich Canonico. – Il secondo sigillo di un proprietario sconosciuto con iniziali: „AB“.")


        $(".naslov").text("HISTRICA")
        $(".izmijena").html('<p> Lo Stancovich acquistava sistematicamente le opere degli autori istriani, ad es. di G. Muzio, G. R. Carli, P. Predonzani, G. Albertini e i volumi in cui si menzionava l\'Istria. Da questi libri e da molti altri che aveva trovato nelle grandi biblioteche italiane attingeva il materiale per la sua grande opera bibliografica “Biografia degli uomini distinti dell\' Istria“ pubblicata a Trieste nel 1828-1829. In essa, accanto a personalità romane e italiane di spicco, figurano anche alcuni Croati istriani, ad es. Franjo Glavinić.</p><p> Andrea Divo / Divus di Capodistria, nel XVI secolo aveva pubblicato le traduzioni latine di Omero, Teocrito e Aristofane. Giovanni Tatio (Tacco), pure di Capodistria che aveva operato nell XVI secolo.</p><p> Giovanni Antonio Pantera (XVI sec.) nato a Cittanova, che aveva ufficiato come canonico nella cattedrale di Parenzo.</p><p> Giacomo Filippo Tomasini (1595-1655), vescovo a Cittanova.</p><p> Franjo Glavinić (Canfanaro 1585 – Tersatto 1652), scrittore, francescano. Ha pubblicato opere in lingua croata, italiana e latina.</p>')
    }
}

$(document).scroll(function() {
    var st = $(this).scrollTop();
    $("#header").css({
        "background-position-y": (-st / 20)
    })
    $("#headerc").css({
        "top": (-st / 5),
        "bottom": (st / 5)
    })
});
/*
window.onscroll = function() {
    myFunction()
};

var navbar = document.getElementById("navbar");
var sidebar = document.getElementById("sidenav");
var sticky = navbar.offsetTop;

function myFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
        sidebar.classList.add("sticky")
    } else {
        navbar.classList.remove("sticky");
        sidebar.classList.remove("sticky");

    }
}
*/
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
    dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var dropdownContent = this.nextElementSibling;
        if (dropdownContent.style.display === "block") {
            dropdownContent.style.display = "none";
        } else {
            dropdownContent.style.display = "block";
        }
    });
}




$(document).mouseup(function(e) {
    var container = $(".slikica");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $("#glass").hide();
    }
});