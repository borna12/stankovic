$("#hr").click(function() {
    localStorage.setItem('jezik', 'hr');
    promijeni_jezik()
})

$("#eng").click(function() {
    localStorage.setItem('jezik', 'eng');
    promijeni_jezik()
})

$("#it").click(function() {
    localStorage.setItem('jezik', 'it');
    promijeni_jezik()
})

$('.parallax-window').parallax({
    imageSrc: 'slike/police.jpg'
});

$('.parallax-window').parallax({

});

$(document).ready(function() {
    promijeni_jezik()

});

function promijeni_jezik() {
    if (localStorage.getItem("jezik") == null) {
        localStorage.setItem('jezik', 'hr')
    }
    if (localStorage.getItem("jezik") == "hr") {

        $("#hr").css({
            "font-weight": "bold",
            "opacity": "1"
        })
        $("#eng").css({
            "font-weight": "normal"
        })


        $("#it").css({
            "font-weight": "normal",
            "opacity": "0.5"
        })

        $(".naznas").text("Naslovnica")
        $(".nazdjela").text("Djela")
        $(".naztis").text("Tiskarski rariteti")
        $(".naz16").text("Rariteti iz 16. stoljeća")
        $(".nazzbi").text("Stancoviciana")
        $(".opisslike").text("Petar Stanković, Autoportret, 1792.")
        $(".nazsta").text("Petar Stanković")
        $(".str").text("str.")
        $(".ilu").text("ilustr.")
        $(".muz").text("Muzej Grada Rovinja – Rovigno")

        $(".naslov").text("CROATICA")
        $(".izmijena").html('<p>Stanković je za svoju knjižnicu nabavljao i knjige na hrvatskom jeziku te nekoliko knjiga hrvatskih pisaca na latinskom jeziku: Josipa Bedekovića, Sebastijana Slade-Dolcija, Fabijana Blaškovića, Ivana Pavlovića Lučića i Klementa Grubišića.</p><p> <strong>ANGELO DALLA COSTA</strong> (1732-1790), objavio je 1778. g. iscrpan priručnik kanonskog prava na hrvatskom jeziku.</p><p> <strong>ŠIME STARČEVIĆ</strong> (1784-1859), objavio je 1812. g. francusku gramatiku s naslovom “Mozin Nova ricsoslovnica iliricsko-francezka“.</p><p> <strong>FRANCESCO MARIA APPENDINI</strong> (1768-1837), talijanski povjesničar/povjesnik i lingvist. Od 1792 do 1835. g. živi u Dubrovniku kao nastavnik i upravnik pijarističke srednje škole. 1808. g. objavio je gramatiku hrvatskoga jezika, uzimajući uglavnom govor Dubrovnika za osnovu svoga djela.</p><p> <strong>IVAN BELOSTENEC</strong> (1593. ili 1594-1675) objavio je latinsko-hrvatski i hrvatsko-latinski rječnik “Gazophylacium“.</p><p> <strong>BEDEKOVIĆ-KOMORSKI, JOSIP</strong> (1688-1760), pavlin; autor je djela “Natale solum“ u kojem je zastupao tezu da je sv. Jeronim Hrvat.</p><p> O pitanju podrijetla sv. Jeronima Stanković je objavio dvije knjige.</p>')

    } else if (localStorage.getItem("jezik") == "eng") {

        $("#hr").css({
            "font-weight": "normal"
        })
        $("#eng").css({
            "font-weight": "bold"
        })

        $("#it").css({
            "font-weight": "normal"
        })

        $(".izmijena").html('<p>PETAR STANKOVIĆ – PIETRO STANCOVICH (Barbana, 24 febbraio 1771 - Barbana, 12 settembre 1852) canonico e storiografo, iniziò il suo lungo percorso scolastico e di studi a Rovigno e grazie alla grande dedizione per l’attività intellettuale e al desiderio di imparare, ebbe durante la sua vita enormi successi nel campo delle scienze. Il suo costante e attivo impegno nello studio lo arricchiva intellettualmente, portandolo anche ad essere membro di numerose accademie.</p><p> Analizzando l\'opera omnia di Stancovich, ovvero i ventitre scritti pubblicati, i suoi manoscritti e i titoli dei libri da lui collezionati con passione per la sua biblioteca, notiamo la varietà degli interessi personali di questo personaggio. I volumi da lui donati alla città di Rovigno illustrano le tendenze della vita sociale, economica e culturale dell\'epoca e i cambiamenti del territorio. Stancovich ci ha pertanto offerto un autentico tesoro per le ricerche e lo studio del patrimonio librario che non ha solo un mero valore storico e pratico, ma anche un significato artistico quale parte dell\'identità civica e nazionale. L’uomo Pietro Stancovich ci ricorda che non esiste vita proficua senza abnegazione.</p>')

    } else {
        $("#hr").css({
            "font-weight": "normal",
            "opacity": "0.5"
        })
        $("#eng").css({
            "font-weight": "normal"
        })

        $("#it").css({
            "font-weight": "bold",
            "opacity": "1"
        })
        $(".naznas").text("Pagina iniziale")
        $(".nazdjela").text("Opere")
        $(".naztis").text("Rarità di stampa")
        $(".naz16").text("Rarità del secolo XVI")
        $(".nazzbi").text("Stancoviciana")
        $(".opisslike").text("Petar Stanković, autoritratto, 1792.")
        $(".nazsta").text("Pietro Stancovich")
        $(".str").text("p.")
        $(".ilu").text("ill.")
        $(".muz").text("Museo della Città di Rovinj – Rovigno")

        $(".naslov").text("CROATICA")
        $(".izmijena").html('<p> Lo Stancovich ha acquistato per la sua biblioteca anche opere in lingua croata e alcuni volumi di scrittori croati in lingua latina: Josip Bedeković, Sebastijan Slade-Dolci, Fabijan Blašković, Ivan Pavlović Lučić e Klemente Grubišić.</p><p> <strong>ANGELO DALLA COSTA </strong> (1732-1790), nel 1778 ha pubblicato un esauriente manuale del diritto canonico in lingua croata.</p><p>  <strong>ŠIME STARČEVIĆ</strong> (1784-1859) nel 1812 aveva pubblicato la grammatica francese intitolata “Mozin Nova ricsoslovnica iliricsko-francezka“.</p><p>  <strong>FRANCESCO MARIA APPENDINI</strong> (1768-1837), storico e linguista italiano. Dal 1792 al 1835 è vissuto a Dubrovnik come insegnante e direttore della scuoala media piaristica. Nel 1808 pubblicò una grammatica della lingua croata, adottando in genere la parlata ragusea come base della sua opera.</p><p>  <strong>IVAN BELOSTENEC</strong> (1593 o 1594-1675) pubblicò un grande vocabolario latino-croato e croato-latino “Gazophylacium“.</p><p>  <strong>BEDEKOVIĆ-KOMORSKI, JOSIP</strong> (1688-1760), paolino, è autore dell\'opera “Natale solum“ in cui ha sostenuto la tesi che S. Gerolamo era croato.</p><p> Sulla questione delle origini di San Gerolamo, Stancovich ha pubblicato due libri.</p>')
    }
}

/*$(document).scroll(function() {
    var st = $(this).scrollTop();
    $("#header").css({
        "background-position-y": (-st / 20)
    })
    $("#headerc").css({
        "top": (-st / 5),
        "bottom": (st / 5)
    })
});

window.onscroll = function() {
    myFunction()
};

var navbar = document.getElementById("navbar");
var sidebar = document.getElementById("sidenav");
var sticky = navbar.offsetTop;

function myFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
        sidebar.classList.add("sticky")
    } else {
        navbar.classList.remove("sticky");
        sidebar.classList.remove("sticky");

    }
}
*/
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
    dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var dropdownContent = this.nextElementSibling;
        if (dropdownContent.style.display === "block") {
            dropdownContent.style.display = "none";
        } else {
            dropdownContent.style.display = "block";
        }
    });
}




$(document).mouseup(function(e) {
    var container = $(".slikica");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $("#glass").hide();
    }
});