$("#hr").click(function() {
    localStorage.setItem('jezik', 'hr');
    promijeni_jezik()
})

$("#eng").click(function() {
    localStorage.setItem('jezik', 'eng');
    promijeni_jezik()
})

$("#it").click(function() {
    localStorage.setItem('jezik', 'it');
    promijeni_jezik()
})

$('.parallax-window').parallax({
    imageSrc: 'slike/police.jpg'
});

$('.parallax-window').parallax({

});

$(document).ready(function() {
    promijeni_jezik()


});

function promijeni_jezik() {
     let searchParams = new URLSearchParams(window.location.search)
    searchParams.has('j') // true
    let param = searchParams.get('j')

    if (param == "hr") {
        localStorage.setItem('jezik', 'hr')
    } else if (param == "it") {
        localStorage.setItem('jezik', 'it')
    } else if (localStorage.getItem("jezik") == null) {
        localStorage.setItem('jezik', 'hr')
    }
    if (localStorage.getItem("jezik") == "hr") {

        $("#hr").css({
            "font-weight": "bold",
            "opacity": "1"
        })
        $("#eng").css({
            "font-weight": "normal"
        })


        $("#it").css({
            "font-weight": "normal",
            "opacity": "0.5"
        })
        $(".naznas").text("Naslovnica")
        $(".nazdjela").text("Djela")
        $(".naztis").text("Tiskarski rariteti")
        $(".naz16").text("Rariteti iz 16. stoljeća")
        $(".nazzbi").text("Stancoviciana")
        $(".naslov").text("Petar Stanković")
        $(".opisslike").html("<em>Luigi Gobbato - Kanonik Petar Stanković<br>1829., ulje na platnu, 57 x 77 cm<br>Muzej Grada Rovinja – Rovigno – Museo della Città di Rovinj – Rovigno</em>")
        $(".demo").attr({
            "data-title": "Luigi Gobbato - Kanonik Petar Stanković<br>1829., ulje na platnu, 57 x 77 cm<br>Muzej Grada Rovinja – Rovigno – Museo della Città di Rovinj – Rovigno<br>"
        })

        $(".nazsta").text("Petar Stanković")

        $(".izmijena").html('<p><strong>PETAR STANKOVIĆ</strong> (24. veljače 1771., Barban - 12. rujna 1852., Barban) kanonik i polihistor, koji svoje školovanje započinje u Rovinju, uz predanost intelektualnom radu i želji za znanjem, kroz život postiže zavidne uspjehe u znanosti. Temeljem svog stalnog i aktivnog angažmana, Stanković se neprestano usavršavao i bio član mnogih akademija.</p><p> Uvidom u Stankovićev plodan opus od dvadeset i tri objavljena djela, rukopise i naslove knjiga koje je strastveno prikupljao za svoju biblioteku, pruža nam se široka paleta njegovih interesa. Knjižnična građa koju oporučno ostavlja gradu Rovinju svjedoči o tendencijama onodobnog društvenog, gospodarskog i kulturnog života te prostornim promjenama. Stanković pruža dobrobit za istraživanje i upoznavanje knjižne baštine, koja ima ne samo povijesne i praktične, već i umjetničke vrijednosti kao dio zavičajnog, a tako i nacionalnog identiteta.</p><p> Ličnost Petra Stankovića podsjeća na činjenicu da nema plodnog života bez samoprijegora.</p>')

    } else if (localStorage.getItem("jezik") == "eng") {

        $("#hr").css({
            "font-weight": "normal"
        })
        $("#eng").css({
            "font-weight": "bold"
        })

        $("#it").css({
            "font-weight": "normal"
        })


        $(".izmijena").html('<p><strong>PETAR STANKOVIĆ</strong> (Barbana, 24 febbraio 1771 - Barbana, 12 settembre 1852) canonico e storiografo, iniziò il suo lungo percorso scolastico e di studi a Rovigno e grazie alla grande dedizione per l’attività intellettuale e al desiderio di imparare, ebbe durante la sua vita enormi successi nel campo delle scienze. Il suo costante e attivo impegno nello studio lo arricchiva intellettualmente, portandolo anche ad essere membro di numerose accademie.</p><p> Analizzando l\'opera omnia di Stancovich, ovvero i ventitre scritti pubblicati, i suoi manoscritti e i titoli dei libri da lui collezionati con passione per la sua biblioteca, notiamo la varietà degli interessi personali di questo personaggio. I volumi da lui donati alla città di Rovigno illustrano le tendenze della vita sociale, economica e culturale dell\'epoca e i cambiamenti del territorio. Stancovich ci ha pertanto offerto un autentico tesoro per le ricerche e lo studio del patrimonio librario che non ha solo un mero valore storico e pratico, ma anche un significato artistico quale parte dell\'identità civica e nazionale. L’uomo Pietro Stancovich ci ricorda che non esiste vita proficua senza abnegazione.</p>')

    } else {
        $("#hr").css({
            "font-weight": "normal",
            "opacity": "0.5"
        })
        $("#eng").css({
            "font-weight": "normal"
        })

        $("#it").css({
            "font-weight": "bold",
            "opacity": "1"
        })
        $(".naznas").text("Pagina iniziale")
        $(".nazdjela").text("Opere")
        $(".naztis").text("Rarità di stampa")
        $(".naz16").text("Rarità del secolo XVI")
        $(".nazzbi").text("Stancoviciana")
        $(".naslov").text("Pietro Stancovich")
        $(".opisslike").html("<em>Luigi Gobbato – Canonico Pietro Stancovich<br>1829, olio su tela, 57 x 77 cm<br>Muzej Grada Rovinja – Rovigno – Museo della Città di Rovinj – Rovigno</em>")
        $(".demo").attr({
            "data-title": "Luigi Gobbato – Canonico Pietro Stancovich<br>1829, olio su tela, 57 x 77 cm<br>Muzej Grada Rovinja – Rovigno – Museo della Città di Rovinj – Rovigno"
        })
        $(".nazsta").text("Pietro Stancovich")

        $(".izmijena").html('<p><strong>PETAR STANKOVIĆ - PIETRO STANCOVICH</strong> (Barbana, 24 febbraio 1771 - Barbana, 12 settembre 1852) canonico e storiografo, iniziò il suo lungo percorso scolastico e di studi a Rovigno e grazie alla grande dedizione per l’attività intellettuale e al desiderio di imparare, ebbe durante la sua vita enormi successi nel campo delle scienze. Il suo costante e attivo impegno nello studio lo arricchiva intellettualmente, portandolo anche ad essere membro di numerose accademie.</p><p> Analizzando l\'opera omnia di Stancovich, ovvero i ventitre scritti pubblicati, i suoi manoscritti e i titoli dei libri da lui collezionati con passione per la sua biblioteca, notiamo la varietà degli interessi personali di questo personaggio. I volumi da lui donati alla città di Rovigno illustrano le tendenze della vita sociale, economica e culturale dell\'epoca e i cambiamenti del territorio. Stancovich ci ha pertanto offerto un autentico tesoro per le ricerche e lo studio del patrimonio librario che non ha solo un mero valore storico e pratico, ma anche un significato artistico quale parte dell\'identità civica e nazionale. L’uomo Pietro Stancovich ci ricorda che non esiste vita proficua senza abnegazione.</p>')
    }
}

$(document).scroll(function() {
    var st = $(this).scrollTop();
    $("#header").css({
        "background-position-y": (-st / 20)
    })
    $("#headerc").css({
        "top": (-st / 5),
        "bottom": (st / 5)
    })
});
/*
window.onscroll = function() {
    myFunction()
};

var navbar = document.getElementById("navbar");
var sidebar = document.getElementById("sidenav");
var sticky = navbar.offsetTop;

function myFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
        sidebar.classList.add("sticky")
    } else {
        navbar.classList.remove("sticky");
        sidebar.classList.remove("sticky");

    }
}*/

var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
    dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var dropdownContent = this.nextElementSibling;
        if (dropdownContent.style.display === "block") {
            dropdownContent.style.display = "none";
        } else {
            dropdownContent.style.display = "block";
        }
    });
}


/*
function touchHandler(event) {
    var touches = event.changedTouches,
        first = touches[0],
        type = "";
    switch (event.type) {
        case "touchstart":
            type = "mousedown";
            break;
        case "touchmove":
            type = "mousemove";
            break;
        case "touchend":
            type = "mouseup";
            break;
        default:
            return;
    }

*/
// initMouseEvent(type, canBubble, cancelable, view, clickCount, 
//                screenX, screenY, clientX, clientY, ctrlKey, 
//                altKey, shiftKey, metaKey, button, relatedTarget);

/*  var simulatedEvent = document.createEvent("MouseEvent");
    simulatedEvent.initMouseEvent(type, true, true, window, 1,
        first.screenX, first.screenY,
        first.clientX, first.clientY, false,
        false, false, false, 0  , null);

    first.target.dispatchEvent(simulatedEvent);
    event.preventDefault();
}*/




$('.slikica').lightzoom({
    glassSize: 175,
    zoomPower: 3
});

$(document).mouseup(function(e) {
    var container = $(".slikica");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $("#glass").hide();
    }
});