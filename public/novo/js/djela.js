$("#hr").click(function() {
    localStorage.setItem('jezik', 'hr');
    promijeni_jezik()
})

$("#eng").click(function() {
    localStorage.setItem('jezik', 'eng');
    promijeni_jezik()
})

$("#it").click(function() {
    localStorage.setItem('jezik', 'it');
    promijeni_jezik()
})

$('.parallax-window').parallax({
    imageSrc: 'slike/police.jpg'
});

$('.parallax-window').parallax({

});

$(document).ready(function() {
    promijeni_jezik()

});

function promijeni_jezik() {
     let searchParams = new URLSearchParams(window.location.search)
    searchParams.has('j') // true
    let param = searchParams.get('j')

    if (param == "hr") {
        localStorage.setItem('jezik', 'hr')
    } else if (param == "it") {
        localStorage.setItem('jezik', 'it')
    } else if (localStorage.getItem("jezik") == null) {
        localStorage.setItem('jezik', 'hr')
    }
    if (localStorage.getItem("jezik") == "hr") {

        $("#hr").css({
            "font-weight": "bold",
            "opacity": "1"
        })


        $("#eng").css({
            "font-weight": "normal"
        })

        $("#it").css({
            "font-weight": "normal",
            "opacity": "0.5"
        })


        $(".naznas").html("Naslovnica")
        $(".nazdjela").html("<span>Djela</span>")
        $(".naztis").html("<span>Tiskarski rariteti</span")
        $(".naz16").html("<span>Rariteti iz 16. stoljeća</span")
        $(".nazzbi").html("<span>Stancoviciana</span")
        $(".opisslike").text("Petar Stanković, Autoportret, 1792.")
        $(".portret").attr({
            "data-title": "Petar Stanković, Autoportret, 1792."
        })
        $(".nazsta").text("Petar Stanković")
        $(".str").text("str.")
        $(".ilu").text("ilustr.")
        $(".muz").text("Muzej Grada Rovinja – Rovigno")
        $(".presav").text("presavijeni list s tablom")
        $(".listab").text("lista s tablama")




        $(".naslov").text("Djela Petra Stankovića")
        $(".izmijena").html('<p><strong>Stanković</strong> je objavio <a href="#sidro" class="tekst_link">23 djela</a> širokog tematskog raspona – od pjesama, znanstvenih djela iz područja arheologije, povijesti, kulturne prošlosti, jezikoslovlja, agronomije – do tehničkih izuma.</p><p> U pismu porečko-puljskom biskupu <strong>A. Peteaniu</strong>, 15. studenoga 1850. g., on navodi: "Već od rane mladosti, potaknut ljubavlju prema domovini, trudio sam se da sakupim sve što mi je moglo doći u ruke ili što mi se ukazalo, a što se odnosilo na domovinu, kako u antičkim spomenicima, tako i u povijesti, znanosti, umjetnosti, agronomiji, geologiji i drugim prirodnim znanostima ..."</p>')

        $(".bibliografija").html("<p id='sidro'><strong>BIBLIOGRAFIJA DJELA PETRA STANKOVIĆA*</strong></p> <img src='slike/grb.png' style='max-width:80px;height:auto'><ul class='smece'> <li> NEOFASTE IN ASTIRI, Versi ed una Novella in prosa intitolata, Venezia, per Picotti, 1818; in 12°</li><li> L'ARATRO SEMINATORE, ossia metodo di piantare il grano arando, con una tavola in rame. Venezia, per Picotti, 1820; in 8°</li><li> DELL'ANFITEATRO DI POLA, dei oradi mormorei del medesimo: nuovi scavi e scoperte; e di alcune epigrafi e figurine inedite dell'Istria, con VIII tavole, Venezia per Picotti 1822; in 8° </li><li> DELLA PATRIA DI SAN GIROLAMO DOTTORE DI SANTA CHIESA, e della lingua Slava relativa allo stesso, colla figura del Santo. Venezia 1824 per Picotti; in 8°</li><li> NUOVO METODO ECONOMICO-PRATICO DI FARE E CONSERVARE IL VINO, con due tavole in rame. Milano per Giovanni Silvestri, 1825; in 8°</li><li> CANZONE CHE SI CANTA NELLE PUBBLICHE ROGAZIONI PER IMPLORARE LA FERTILITÀ DELLA TERRA, dal Tedesco volta in Italiano, Venezia per Picotti 1825; in 8°</li><li> ALLOCUZIONE NELL'OCCASIONE DI VISITA PATORALE BALBI, Venezia per Picotti 1826; in 8°</li><li> KRATAK NAUK KARSTJANSKI, Trieste dalla Tipografia Marenigh 1828; in 16°</li><li> BIOGRAFIA DEGLI UOMINI DISTINTI DELL'ISTRIA, Tomi III, Trieste presso Gio, Marenigh 1828-29; in 8° con 8 rami</li><li> SAN GIROLAMO ILI DOTTORE MASSIMO DIMOSTRATO EVIDENTEMENTE DI PATRIA ISTRIANO, apologia alia risposta di D. Giovanni Capor Dalmatino, Trieste 1829, Tipografia Marenigh.</li><li> TRIESTE NON FU VILLAGGIO CARNICO, MA LUOGO DELL'ISTRIA, FORTEZZA E COLONIA DE'CITTADINI ROMANI, Venezia 1830; in 8°</li><li> MARMO DI LUCIO MENACIO PRISCO PATRONO DI POLA, inserto nell'Archeografo Triestino. Vol. II, n. VIII, Trieste tipi Marenigh, 1831.</li><li> L'ANDROGINO, per Nozze, Favoletta di Platone, Venezia, Picotti, Febraio 1832.</li><li> DIALOGHI CRITICI SERO-FACETI DI VERANZIO ISTINA DALAMTINO CON ANDREA MORETTO DETTO MEMORIA. Venezia 1833, Alvisopoli.</li><li> DEPOSITI DI MONETE UNGHERESI, CARRARESI E VENEZIANE, scoperto in Istria con Tavola litografica, inserto nel Vol. Ill dell'Archeografo. Trieste, 1833.</li><li> DELLE TRE EMONE ANTICHE CITTÀ E COLONIE ROMANE, e della genuina Epigrafe di Cajo precellio patron della splendidissima Colonia degli Aquillejesi, dei Parentini, degli Opitergini, e degli Emonesi. Venezia, Picoti 1835; in 8°</li><li> DEGLI ALTARI E DELLA LORO CONSACRAZIONE, ESECRAZIONE E VIOLAZIONE: e se le Relique siano necessarie nella Consacrazione degli Altari. Venezia, Molinari 1837; in 8°</li><li> SPOLPOLIVA E MACINOCCIOLO, Torino, Stamperia reale, 1840.</li><li> TORCHIOLIVA, OSSIA TORCHIO OLEARIAO, Firenze, Giovanni Mazzani, 1841.</li><li> IL FORMENTO SEMINATO SENZA ARATURA, zappatura, vangatura, eripicatura e senza letame animale, Padova, Minerva, 1842.</li><li> DEGLI ACQUEDOTTI DI ROMA ANTICA E MODERNA ecc., Venezia, Sebastiano Tondelli, 1844.</li><li> DELL'ANTICO ROMANO ARCO ACQUEDOTTO DETTO ARCO RICCARDO o prigione di Riccardo esistente in Trieste, Giov. Marenigh, Trieste, 1846.</li><li> VINO DELL'ISTRIA, METODO PER FARLO. S. Silvestri, Milano, 1853.</li> </ul> <span class='sitnica'>* DOMENICO CERNECCA, Petar Stankovic. Jadranski zbornik, 4 (1959-60).</span>")

    } else if (localStorage.getItem("jezik") == "eng") {

        $("#hr").css({
            "font-weight": "normal"
        })
        $("#eng").css({
            "font-weight": "bold"
        })

        $("#it").css({
            "font-weight": "normal"
        })

        $(".naslov").text("Opere di Pietro Stancovich")
        $(".izmijena").html('<p><strong>PETAR STANKOVIĆ – PIETRO STANCOVICH</strong> (Barbana, 24 febbraio 1771 - Barbana, 12 settembre 1852) canonico e storiografo, iniziò il suo lungo percorso scolastico e di studi a Rovigno e grazie alla grande dedizione per l’attività intellettuale e al desiderio di imparare, ebbe durante la sua vita enormi successi nel campo delle scienze. Il suo costante e attivo impegno nello studio lo arricchiva intellettualmente, portandolo anche ad essere membro di numerose accademie.</p><p> Analizzando l\'opera omnia di Stancovich, ovvero i ventitre scritti pubblicati, i suoi manoscritti e i titoli dei libri da lui collezionati con passione per la sua biblioteca, notiamo la varietà degli interessi personali di questo personaggio. I volumi da lui donati alla città di Rovigno illustrano le tendenze della vita sociale, economica e culturale dell\'epoca e i cambiamenti del territorio. Stancovich ci ha pertanto offerto un autentico tesoro per le ricerche e lo studio del patrimonio librario che non ha solo un mero valore storico e pratico, ma anche un significato artistico quale parte dell\'identità civica e nazionale. L’uomo Pietro Stancovich ci ricorda che non esiste vita proficua senza abnegazione.</p>')

    } else {
        $("#hr").css({
            "font-weight": "normal",
            "opacity": "0.5"
        })
        $("#eng").css({
            "font-weight": "normal"
        })

        $("#it").css({
            "font-weight": "bold",
            "opacity": "1"
        })
        $(".naslov").text("Opere di Pietro Stancovich")
        $(".naznas").text("Pagina iniziale")
        $(".nazdjela").text("Opere")
        $(".naztis").text("Rarità di stampa")
        $(".naz16").text("Rarità del secolo XVI")
        $(".nazzbi").text("Stancoviciana")
        $(".opisslike").text("Pietro Stankovich, Autoritratto, 1792.")

        $(".portret").attr({
            "data-title": "Pietro Stankovich, Autoritratto, 1792."
        })
        $(".nazsta").text("Pietro Stancovich")
        $(".str").text("p.")
        $(".ilu").text("ill.")
        $(".muz").text("Museo della Città di Rovinj – Rovigno")
        $(".presav").text("folio ripiegato con tavola")
        $(".listab").text("fogli con tavola")


        $(".izmijena").html('<p><strong>Lo Stancovich</strong> ha pubblicato <a href="#sidro"href="#sidro" class="tekst_link">23 opere</a> di notevole ampiezza tematica, dalle poesie, opere scientifiche nel campo dell\'archeologia, della storia, del passato culturale, della linguistica, della agronomia, alle invezioni techniche.</p><p> Nella lettera al vescovo parentino-polese <strong>A. Peteani</strong> del 15 novembre 1850 rileva: "Sino dalla mia prima gioventù, animato dall\'amore di patria, io mi prestava a raccogliere tutto ciò che mi poteva pervenire alle mani, o presentare agli occhi, che riguardar potesse la menesima, tanto nelle antichità monumentali, come nella parte storica, scientifica, artistica, agronomica, geologica, ad altre scienze naturali..."')
        $(".bibliografija").html("<p id='sidro'><strong>BIBLIOGRAFIA DELLE OPERE DI PIETRO STANCOVICH*</strong></p><img src='slike/grb.png' style='max-width:80px;height:auto'><ul class='smece'><li>NEOFASTE IN ASTIRI, Versi ed una Novella in prosa intitolata, Venezia, per Picotti, 1818; in 12°</li><li>L'ARATRO SEMINATORE, ossia metodo di piantare il grano arando, con una tavola in rame. Venezia, per Picotti, 1820; in 8°</li><li>DELL'ANFITEATRO DI POLA, dei oradi mormorei del medesimo: nuovi scavi e scoperte; e di alcune epigrafi e figurine inedite dell'Istria, con VIII tavole, Venezia per Picotti 1822; in 8° </li><li>DELLA PATRIA DI SAN GIROLAMO DOTTORE DI SANTA CHIESA, e della lingua Slava relativa allo stesso, colla figura del Santo. Venezia 1824 per Picotti; in 8°</li><li>NUOVO METODO ECONOMICO-PRATICO DI FARE E CONSERVARE IL VINO, con due tavole in rame. Milano per Giovanni Silvestri, 1825; in 8°</li><li>CANZONE CHE SI CANTA NELLE PUBBLICHE ROGAZIONI PER IMPLORARE LA FERTILITÀ DELLA TERRA, dal Tedesco volta in Italiano, Venezia per Picotti 1825; in 8°</li><li>ALLOCUZIONE NELL'OCCASIONE DI VISITA PATORALE BALBI, Venezia per Picotti 1826; in 8°</li><li>KRATAK NAUK KARSTJANSKI, Trieste dalla Tipografia Marenigh 1828; in 16°</li><li>BIOGRAFIA DEGLI UOMINI DISTINTI DELL'ISTRIA, Tomi III, Trieste presso Gio, Marenigh 1828-29; in 8° con 8 rami</li><li>SAN GIROLAMO ILI DOTTORE MASSIMO DIMOSTRATO EVIDENTEMENTE DI PATRIA ISTRIANO, apologia alia risposta di D. Giovanni Capor Dalmatino, Trieste 1829, Tipografia Marenigh.</li><li>TRIESTE NON FU VILLAGGIO CARNICO, MA LUOGO DELL'ISTRIA, FORTEZZA E COLONIA DE'CITTADINI ROMANI, Venezia 1830; in 8°</li><li>MARMO DI LUCIO MENACIO PRISCO PATRONO DI POLA, inserto nell'Archeografo Triestino. Vol. II, n. VIII, Trieste tipi Marenigh, 1831.</li><li>L'ANDROGINO, per Nozze, Favoletta di Platone, Venezia, Picotti, Febraio 1832.</li><li>DIALOGHI CRITICI SERO-FACETI DI VERANZIO ISTINA DALAMTINO CON ANDREA MORETTO DETTO MEMORIA. Venezia 1833, Alvisopoli.</li><li>DEPOSITI DI MONETE UNGHERESI, CARRARESI E VENEZIANE, scoperto in Istria con Tavola litografica, inserto nel Vol. Ill dell'Archeografo. Trieste, 1833.</li><li>DELLE TRE EMONE ANTICHE CITTÀ E COLONIE ROMANE, e della genuina Epigrafe di Cajo precellio patron della splendidissima Colonia degli Aquillejesi, dei Parentini, degli Opitergini, e degli Emonesi. Venezia, Picoti 1835; in 8°</li><li>DEGLI ALTARI E DELLA LORO CONSACRAZIONE, ESECRAZIONE E VIOLAZIONE: e se le Relique siano necessarie nella Consacrazione degli Altari. Venezia, Molinari 1837; in 8°</li><li>SPOLPOLIVA E MACINOCCIOLO, Torino, Stamperia reale, 1840.</li><li>TORCHIOLIVA, OSSIA TORCHIO OLEARIAO, Firenze, Giovanni Mazzani, 1841.</li><li>IL FORMENTO SEMINATO SENZA ARATURA, zappatura, vangatura, eripicatura e senza letame animale, Padova, Minerva, 1842.</li><li>DEGLI ACQUEDOTTI DI ROMA ANTICA E MODERNA ecc., Venezia, Sebastiano Tondelli, 1844.</li><li>DELL'ANTICO ROMANO ARCO ACQUEDOTTO DETTO ARCO RICCARDO o prigione di Riccardo esistente in Trieste, Giov. Marenigh, Trieste, 1846.</li><li>VINO DELL'ISTRIA, METODO PER FARLO. S. Silvestri, Milano, 1853.</li></ul><span class='sitnica'>* DOMENICO CERNECCA, Petar Stankovic. Jadranski zbornik, 4 (1959-60).</span>")
    }
}

$(document).scroll(function() {
    var st = $(this).scrollTop();
    $("#header").css({
        "background-position-y": (-st / 20)
    })
    $("#headerc").css({
        "top": (-st / 5),
        "bottom": (st / 5)
    })
});
/*
window.onscroll = function() {
    myFunction()
};

var navbar = document.getElementById("navbar");
var sidebar = document.getElementById("sidenav");
var sticky = navbar.offsetTop;

function myFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
        sidebar.classList.add("sticky")
    } else {
        navbar.classList.remove("sticky");
        sidebar.classList.remove("sticky");

    }
}

var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
    dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var dropdownContent = this.nextElementSibling;
        if (dropdownContent.style.display === "block") {
            dropdownContent.style.display = "none";
        } else {
            dropdownContent.style.display = "block";
        }
    });
}

*/


$(document).mouseup(function(e) {
    var container = $(".slikica");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $("#glass").hide();
    }
});