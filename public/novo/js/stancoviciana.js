$("#it").click(function() {
    localStorage.setItem('jezik', 'it');
    promijeni_jezik()
})

$('.parallax-window').parallax({
    imageSrc: 'slike/police.jpg'
});

$('.parallax-window').parallax({

});



function promijeni_jezik() {
     let searchParams = new URLSearchParams(window.location.search)
    searchParams.has('j') // true
    let param = searchParams.get('j')

    if (param == "hr") {
        localStorage.setItem('jezik', 'hr')
    } else if (param == "it") {
        localStorage.setItem('jezik', 'it')
    } else if (localStorage.getItem("jezik") == null) {
        localStorage.setItem('jezik', 'hr')
    }
    if (localStorage.getItem("jezik") == "hr") {

        $("#hr").css({
            "font-weight": "bold",
            "opacity": "1"
        })
        $("#eng").css({
            "font-weight": "normal"
        })
        $("#it").css({
            "font-weight": "normal",
            "opacity": "0.5"
        })
        $(".naznas").text("Naslovnica")
        $(".nazdjela").text("Djela")
        $(".naztis").text("Tiskarski rariteti")
        $(".naz16").text("Rariteti iz 16. stoljeća")
        $(".nazzbi").text("Stancoviciana")
        $(".nazsta").text("Petar Stanković")

        $(".naslov").text("Stancoviciana")

        $("#rart").text("Tiskarski rariteti")
        $("#rar16").text("Rariteti iz 16. stoljeća")
        $(".sadrzaj").html("<p> Pismo Petra Stankovića,</p><p align='center'> <em> Njegovu Gospodstvu Grofu Luigiu Pisaniu, Venecija – Barban, 7. prosinca 1836. </em></p><p align='center'> <em> „.... 18. kolovoza sačinio sam veoma dugu oporuku, podijeljenu na 10 poglavlja. Vama su već od prije poznate moje nakane darivanja, a uključene su u oporuku. Obrazovanje i knjižnica su njezina dva glavna dijela. 4000 florina sam već odredio za prvu, a knjižnica ima ostati Javna knjižnica na dar gradu Rovinju. Dar nije male vrijednosti, jer među 3000 primjeraka po njihovoj kvaliteti, sadržaju i rijetkosti, ima knjiga znatne vrijednosti. Ovoj se zbirci pridružuju zbirke mramora, okamina, minerala i instrumenata, što sve čini lijepi dar Istri. </em></p><p align='center'> <em> .... prije nego vidim uništene, potrgane i izgubljene ove knjige, koje su me stajale puno novaca, koje su bile i jesu jedina moja zabava, i koje čuvam kao zjenice svojih očiju, odlučio sam ih darovati Rovinju da se sačuva.“ </em></p>");

        //$(".content").html('')

    } else if (localStorage.getItem("jezik") == "eng") {

        $("#hr").css({
            "font-weight": "normal"
        })
        $("#eng").css({
            "font-weight": "bold"
        })

        $("#it").css({
            "font-weight": "normal"
        })


        $(".content").html('<p> <strong>Museo della Città di Rovinj-Rovigno</strong></p><p> <strong> </strong></p><p> <strong>Biblioteca memoriale Stancoviciana</strong></p><p> <strong> </strong></p><p> <strong> </strong></p><p> <strong>EDITORE:</strong></p><p> <ul><li class="smece">Museo della Città di Rovinj-Rovigno</li></ul></p><p> <strong>PER L\'EDITORE:</strong></p><p><ul><li class="smece"> Marija Smolica</ul></li></p><p> <strong>AUTRICI DELLA MOSTRA:</strong></p><p> Andrea Milotić, Marija Smolica</p><p> <strong> </strong></p><p> <strong>DESIGN E PROGRAMMAZIONE:</strong></p><p> Link2 d.o.o.</p><p> <strong> </strong></p><p> <strong>DIGITALIZZAZIONE DEL MATERIALE:</strong></p><p> Andrea Milotić</p><p> <strong> </strong></p><p> <strong>TRADUZIONE ITALIANA:</strong></p><p> Atinianum d.o.o. Vodnjan-Dignano</p><p> <strong> </strong></p><p> <strong> Fondamento teorico primario della realizzazione della mostra virtuale: </strong></p><p> Stancoviciana : catalogo della mostra: Museo civico di Rovigno / [testo del catalogo; biografia Petra Stankovića Miroslav Bertoša ; traduzioni Eufemia Papić, Silvana Kos]. Rovinj: Museo civico di Rovigno, 1992.</p><p> <strong> La mostra virtuale è stata finanziata con i mezzi della Città di Rovinj </strong> <strong>-</strong> <strong> Rovigno</strong></p>')

    } else {
        $("#hr").css({
            "font-weight": "normal",
            "opacity": "0.5"
        })
        $("#eng").css({
            "font-weight": "normal"
        })

        $("#it").css({
            "font-weight": "bold",
            "opacity": "1"
        })
        $(".naslov").text("Stancoviciana")

        $(".naznas").text("Pagina iniziale")
        $(".nazdjela").text("Opere")
        $(".naztis").text("Rarità di stampa")
        $(".naz16").text("Rarità del secolo XVI")
        $(".nazzbi").text("Stancoviciana")
        $("#rart").text("Rarità di stampa")
        $("#rar16").text("Rarità del secolo XVI")
        $(".sadrzaj").html("<p> Lettera di Pietro Stancovich</p><p align='center'> Al nobile sign. Conte Luigi Pisani, Venezia – Barban, 7. Dicembre 1836.</p><p align='center'> <em>Nel 18 Agosto (...) io feci il mio testamento lungo lungo, divizo in dieci capitoli. Ella se le mie intenzioni di pubblica beneficenza, ed il testamento le include. L'educazione, e la librariane sono i due principali ogetti. Fiorini 4000 per la prima furono da me disposti, e la libraria per principio di Pubblica Libraria in dono alla città di Rovigno. Il dono non e da poco, perche 3000 volumi fra quali, e per qualità, e per l'argomento, e per rarità ve ne sono rimarcabile valore. A questa si agiungono la collezione di marmi, di netrfatti, de minerali, ed instrumenti, el che tutto forma un bel dono per l'Istria.</em></p><p align='center'> <em>(...) piuttosto di vederedistrutti, strazzati, e perduti questi libri, che tanti denari a me costavano, che hanno formato il mio unico intrattenimento e che sono da me riguardati come la puppila degli occhi, indussiero a donarla a Rovinjo, perche siano conservati.“</em></p>");
        $(".nazsta").text("Pietro Stancovich")

    }
}

$(document).scroll(function() {
    var st = $(this).scrollTop();
    $("#header").css({
        "background-position-y": (-st / 20)
    })
    $("#headerc").css({
        "top": (-st / 5),
        "bottom": (st / 5)
    })
});
/*
window.onscroll = function() {
    myFunction()
};

var navbar = document.getElementById("navbar");
var sidebar = document.getElementById("sidenav");
var sticky = navbar.offsetTop;

function myFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
        sidebar.classList.add("sticky")
    } else {
        navbar.classList.remove("sticky");
        sidebar.classList.remove("sticky");

    }
}

var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
    dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var dropdownContent = this.nextElementSibling;
        if (dropdownContent.style.display === "block") {
            dropdownContent.style.display = "none";
        } else {
            dropdownContent.style.display = "block";
        }
    });
}*/