$("#hr").click(function() {
    localStorage.setItem('jezik', 'hr');
    promijeni_jezik()
})

$("#eng").click(function() {
    localStorage.setItem('jezik', 'eng');
    promijeni_jezik()
})

$("#it").click(function() {
    localStorage.setItem('jezik', 'it');
    promijeni_jezik()
})

$("#pageHr").click(function() {
    $('html,body').animate({
            scrollTop: $("#navbar").offset().top
        },
        'slow');
})


$(document).ready(function() {
    promijeni_jezik()
});

function promijeni_jezik() {
    if (localStorage.getItem("jezik") == null) {
        localStorage.setItem('jezik', 'it')
    }
    if (localStorage.getItem("jezik") == "hr") {

        $("#hr").css({
            "font-weight": "bold",
            "opacity": "1"
        })
        $("#eng").css({
            "font-weight": "normal"
        })


        $("#it").css({
            "font-weight": "normal",
            "opacity": "0.5"
        })
        $(".naznas").text("Naslovnica")
        $(".nazdjela").text("Djela")
        $(".naztis").text("Tiskarski rariteti")
        $(".naz16").text("Rariteti iz 16. stoljeća")
        $(".nazzbi").text("Stancoviciana")
        $(".naslov").text("Naslovnica")
                $(".nazsta").text("Petar Stanković")


        $(".content").html('<h1>Spomenička knjižnica Stancoviciana</h1> <img src="slike/grb.png" style="max-width:100px;height:auto"><p> Virtualna izložba priređena je na osnovi kataloga izložbe <em>Stancoviciana </em>iz 1992. u izdanju Muzeja Grada Rovinja – Rovigno – Museo della Città di Rovinj – Rovigno. Primarno polazište mrežno utemeljene izložbe, uspostavljene oko predstavljanja Spomeničke knjižnice "Stancoviciana", izrada je dinamičnog proizvoda usmjerenog korisnicima u otkrivanju i prepoznavanju rovinjske kulturne baštine. Odabrani izlošci organizirani su oko tematskih cjelina<em> Djela Petra Stankovića, Histrica, Croatica, Tiskarski rariteti</em>&nbsp;i <em>Rariteti iz 16. stoljeća.</em> Izbor knjižnične građe temelji se na raritetnosti, očuvanosti i sadržajnoj vrijednosti. Izložba je koncipirana informativnim tekstovima i ilustrativnim materijalom. Posebnost izložbe ogleda se i u kontekstu budućeg nadograđivanja sadržaja novim pristupima u virtualnom prostoru i povezivanju s novom publikom. </p><p> Knjižnica Muzeja Grada Rovinja – Rovigno – Museo della Città di Rovinj – Rovigno svoj postanak veže uz osnivanje muzeja 1954. godine u palači Califfi. Nekadašnja obiteljska palača izrasla u rovinjskoj starogradskoj jezgri sačuvala je prvotne strukture gradnje uz dominantni barokni izričaj. Pozicioniranje muzeja nadomak Trga na mostu, nekadašnjeg povezivanja otočnog naselja s kopnom, i danas predstavlja žarište u urbanom tkivu grada. </p><p> Muzejska knjižnica, kao sastavni dio Muzeja, specifična je vrsta specijalne knjižnice ponajprije zbog svog baštinskog karaktera. Obimna i raznolika knjižnična građa organizirana je u nekoliko zbirki. Spomenička knjižnica "Stancoviciana" je zbirka koja sadrži 7045 jedinica knjižnične građe. Knjižnični fond počiva na ostacima nekadašnje rovinjske Gradske knjižnice - <em>Biblioteca Comunale</em> koja je utemeljena 1859. godine. Ishodište osnivanja tadašnje Gradske knjižnice počiva u donaciji Petra Stankovića - <em>Pietro Stancovich</em> (1771-1852) koji oporučno Gradu Rovinju ostavlja vlastitu knjižnicu s oko 3000 knjiga. Knjižnični fond iste bogatile su donacije mnogih rovinjskih uglednika i ostaci manjih rovinjskih knjižnica koje su djelovale tijekom druge polovine 19. i prve polovine 20. stoljeća. Danas se u "Stancoviciani" nalazi 2896 svezaka knjiga koje nose vlasnikov ex libris <em>Pietro Stancovich Canonico</em>. </p><p> Sadržajno raznolik fond obuhvaća knjižničnu građu najzastupljeniju iz područja teologije, talijanskog jezika i književnosti, latinskih i grčkih klasika, geografije, astronomije, povijesti, etnografije, umjetnosti, prirodnih znanosti, medicine, arhitekture. Knjižnična građa pisana je uglavnom na talijanskom i latinskom jeziku te manji broj naslova na hrvatskom, grčkom, njemačkom i španjolskom jeziku. Vremenski okvir knjižnične građe je od 1501. do 1852. godine. Zbirka pohranjuje raritetne knjižnične primjerke te je 1986. godine upisana u Registar pokretnih spomenika kulture. Stancoviciana ima svojevrsni status ikone grada Rovinja.</p><img src="slike/ilustracija_2_1200_dpi.jpg" style="display:block; margin:auto; max-width:100%; height:auto">')

        $(".spom").text("Spomenička knjižnica")


    } else if (localStorage.getItem("jezik") == "eng") {

        $("#hr").css({
            "font-weight": "normal"
        })
        $("#eng").css({
            "font-weight": "bold"
        })

        $("#it").css({
            "font-weight": "normal"
        })


        $(".content").html('<h1> Biblioteca memoriale Stancoviciana</h1><p> La mostra virtuale è stata allestita prendendo spunto dal catalogo di quella del 1992, intitolata <em>Stancoviciana,</em> edito dal Muzej Grada Rovinja – Rovigno – Museo della Città di Rovinj – Rovigno. Il punto di partenza primario della mostra che si presenta in rete, curata quale presentazione della Biblioteca memoriale „Stancoviciana“, è l\'elaborazione di un prodotto dinamico rivolto all\'utenza, alla quale viene così offerta l’opportunità di scoprire e riconoscere il patrimonio culturale rovignese. Il materiale viene espostoin insiemi tematici: <em>Le opere di Pietro Stancovich</em>, <em>Histrica, Croatica, Stampe rare e Rarità del XVI secolo, </em> ed è stato scelto con il criterio del suo valore intrinseco considerandone la rarità, lo stato di conservazione e il contenuto. Il concetto della mostra racchiude testi informativi e materiale illustrativo, e la sua peculiarità sta nella possibilità di un futuro ampliamento del contenuto mediante nuove modalità di accesso allo spazio virtuale, le quali porteranno al coinvolgimento di nuovo pubblico.</p><p>La nascita della Biblioteca del Muzej Grada Rovinja – Rovigno – Museo della Città di Rovinj – Rovigno si collega all\'apertura del Museo a Palazzo Califfi nel lontano 1954. Quella che in passato era una residenza familiare del nucleo storico rovignese, ha mantenuto la sua struttura architettonica originale, di fattura prevalentemente barocca. La posizione del Museo a due passi da piazza al Ponte, antico punto di collegamento dell\'abitato insulare con la terraferma, costituisce tuttora il cuore del tessuto urbano cittadino.</p><p> La Biblioteca memoriale, parte integrante del Museo, è un tipo specifico e particolare di biblioteca grazie innanzitutto al suo carattere patrimoniale. L\'eterogeneo e abbondante materiale che vi si custodisce è suddiviso in alcune collezioni. La Biblioteca memoriale "Stancoviciana" è una raccolta di ben 7045 unità bibliografiche. Il suo fondo librario nasce da quelli che erano i resti della vecchia Biblioteca Comunale, fondata nel 1859, che a sua volta ebbe origine dalla donazione di Pietro Stancovich (1771-1852), il quale lasciò in eredità la sua biblioteca personale di circa tremila volumi alla città di Rovigno. Il fondo librario si arricchì poi negli anni grazie alle donazioni di molti emeriti personaggi rovignesi e con i volumi delle piccole biblioteche locali aperte nella seconda metà del XIX secolo e nella prima del XX. Oggi la „Stancoviciana“ custodisce2896 volumi, tutti contrassegnati con l\'<em>ex libris</em> <em>Pietro Stancovich Canonico</em>.</p><p> Eterogeneo per contenuto, il fondo librario comprende prevalentemente testi di carattere teologico, sulla lingua e sulla letteratura italiane, classici latini e greci, libri di geografia, astronomia, storia, etnografia, arte, scienze naturali, medicina, architettura. Si tratta di materiale scritto soprattutto in lingua italiana e latina, mentre un numero minore di libri è in lingua croata, greca, tedesca e spagnola. Il fondo librario appartiene all’epoca compresa tra gli anni 1501 e 1852. La raccolta vanta alcuni volumi rari, e pertanto dal 1986 è iscritta nel Registro dei monumenti materiali mobili. La “Stancoviciana” si è meritata l\'appellativo di icona della città di Rovigno.</p>')
        $(".naslov").text("Biblioteca memoriale")


    } else {
        $("#hr").css({
            "font-weight": "normal",
            "opacity": "0.5"
        })
        $("#eng").css({
            "font-weight": "normal"
        })

        $("#it").css({
            "font-weight": "bold",
            "opacity": "1"
        })
        $(".spom").text("Biblioteca memoriale")

        $(".naznas").text("Pagina iniziale")
        $(".nazdjela").text("Opere")
        $(".naztis").text("Rarità di stampa")
        $(".naz16").text("Rarità del secolo XVI")
        $(".nazzbi").text("Stancoviciana")
        $(".naslov").text("Pagina iniziale")

        $(".nazsta").text("Pietro Stancovich")

        $(".content").html('<h1> Biblioteca memoriale Stancoviciana</h1><img src="slike/grb.png" style="max-width:100px;height:auto"><p> La mostra virtuale è stata allestita prendendo spunto dal catalogo di quella del 1992, intitolata <em>Stancoviciana,</em> edito dal Muzej Grada Rovinja – Rovigno – Museo della Città di Rovinj – Rovigno. Il punto di partenza primario della mostra che si presenta in rete, curata quale presentazione della Biblioteca memoriale „Stancoviciana“, è l\'elaborazione di un prodotto dinamico rivolto all\'utenza, alla quale viene così offerta l’opportunità di scoprire e riconoscere il patrimonio culturale rovignese. Il materiale viene espostoin insiemi tematici: <em>Le opere di Pietro Stancovich</em>, <em>Histrica, Croatica, Stampe rare e Rarità del XVI secolo, </em> ed è stato scelto con il criterio del suo valore intrinseco considerandone la rarità, lo stato di conservazione e il contenuto. Il concetto della mostra racchiude testi informativi e materiale illustrativo, e la sua peculiarità sta nella possibilità di un futuro ampliamento del contenuto mediante nuove modalità di accesso allo spazio virtuale, le quali porteranno al coinvolgimento di nuovo pubblico.</p><p> La nascita della Biblioteca del Muzej Grada Rovinja –  Rovigno – Museo della Città di Rovinj –  Rovigno si collega all\'apertura del Museo a Palazzo Califfi nel lontano 1954. Quella che in passato era una residenza familiare del nucleo storico rovignese, ha mantenuto la sua struttura architettonica originale, di fattura prevalentemente barocca. La posizione del Museo a due passi da piazza al Ponte, antico punto di collegamento dell\'abitato insulare con la terraferma, costituisce tuttora il cuore del tessuto urbano cittadino.</p><p> La Biblioteca memoriale, parte integrante del Museo, è un tipo specifico e particolare di biblioteca grazie innanzitutto al suo carattere patrimoniale. L\'eterogeneo e abbondante materiale che vi si custodisce è suddiviso in alcune collezioni. La Biblioteca memoriale "Stancoviciana" è una raccolta di ben 7045 unità bibliografiche. Il suo fondo librario nasce da quelli che erano i resti della vecchia Biblioteca Comunale, fondata nel 1859, che a sua volta ebbe origine dalla donazione di Pietro Stancovich (1771-1852), il quale lasciò in eredità la sua biblioteca personale di circa tremila volumi alla città di Rovigno. Il fondo librario si arricchì poi negli anni grazie alle donazioni di molti emeriti personaggi rovignesi e con i volumi delle piccole biblioteche locali aperte nella seconda metà del XIX secolo e nella prima del XX. Oggi la „Stancoviciana“ custodisce2896 volumi, tutti contrassegnati con l\'<em>ex libris</em> <em>Pietro Stancovich Canonico</em>.</p><p> Eterogeneo per contenuto, il fondo librario comprende prevalentemente testi di carattere teologico, sulla lingua e sulla letteratura italiane, classici latini e greci, libri di geografia, astronomia, storia, etnografia, arte, scienze naturali, medicina, architettura. Si tratta di materiale scritto soprattutto in lingua italiana e latina, mentre un numero minore di libri è in lingua croata, greca, tedesca e spagnola. Il fondo librario appartiene all’epoca compresa tra gli anni 1501 e 1852. La raccolta vanta alcuni volumi rari, e pertanto dal 1986 è iscritta nel Registro dei monumenti materiali mobili. La “Stancoviciana” si è meritata l\'appellativo di icona della città di Rovigno.</p><img src="slike/ilustracija_2_1200_dpi.jpg" style="display:block; margin:auto; max-width:100%; height:auto">')
    }
}

$(document).scroll(function() {
    var st = $(this).scrollTop();
    $(".pageHr").hide();
    $("#header").css({
        "background-position-y": (-st / 20)
    })
    $("#headerc").css({
        "top": (-st / 5),
        "bottom": (st / 5)
    })
});


window.onscroll = function() {
    myFunction()

};

var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function myFunction() {
    if (window.pageYOffset >= sticky) {

        $('#pageHr').hide();
    } else {
       

        $('#pageHr').show();

    }
}



var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
    dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var dropdownContent = this.nextElementSibling;
        if (dropdownContent.style.display === "block") {
            dropdownContent.style.display = "none";
        } else {
            dropdownContent.style.display = "block";
        }
    });
}