$("#hr").click(function() {
    localStorage.setItem('jezik', 'hr');
    promijeni_jezik()
})

$("#eng").click(function() {
    localStorage.setItem('jezik', 'eng');
    promijeni_jezik()
})

$("#it").click(function() {
    localStorage.setItem('jezik', 'it');
    promijeni_jezik()
})

$('.parallax-window').parallax({
    imageSrc: 'slike/police.jpg'
});

$('.parallax-window').parallax({

});

$(document).ready(function() {
    promijeni_jezik()

});

function promijeni_jezik() {
     let searchParams = new URLSearchParams(window.location.search)
    searchParams.has('j') // true
    let param = searchParams.get('j')

    if (param == "hr") {
        localStorage.setItem('jezik', 'hr')
    } else if (param == "it") {
        localStorage.setItem('jezik', 'it')
    } else if (localStorage.getItem("jezik") == null) {
        localStorage.setItem('jezik', 'hr')
    }
    if (localStorage.getItem("jezik") == "hr") {

        $("#hr").css({
            "font-weight": "bold",
            "opacity": "1"
        })
        $("#eng").css({
            "font-weight": "normal"
        })


        $("#it").css({
            "font-weight": "normal",
            "opacity": "0.5"
        })

        $(".naznas").text("Naslovnica")
        $(".nazdjela").text("Djela")
        $(".naztis").text("Tiskarski rariteti")
        $(".naz16").text("Rariteti iz 16. stoljeća")
        $(".nazzbi").text("Stancoviciana")
        $(".opisslike").text("Petar Stanković, Autoportret, 1792.")
        $(".nazsta").text("Petar Stanković")
        $(".str").text("str.")
        $(".list").text("list")
        $(".listova").text("listova")
        $(".ilu").text("ilustr.")
        $(".muz").text("Muzej Grada Rovinja – Rovigno")


        $(".naslov").text("RARITETI IZ 16. STOLJEĆA")
        $(".izmijena").html('<p> Stanković je nabavio djela gotovo svih značajnih klasičnih rimskih i kršćanskih pisaca. Najstarija knjiga u njegovoj knjižnici djelo je Eusebija iz 1501. godine. Plinijevo djelo “Prirodoslovlje“ (Naturalis historia) jedan je od prvih enciklopedijskih kompendija u Europi, često objavljivan i prevođen. Stanković se i sam bavio prirodnim znanostima, naročito zoologijom i botanikom, i nabavljao knjige takva sadržaja.</p><p> Svetonijevo djelo “Život careva“, koje sadrži životopise 12 rimskih careva, znatno je utjecalo na pisanje mnogih životopisa od srednjeg vijeka nadalje. Ovo, kao i drugo Svetonijevo djelo “Znameniti muževi“ (Viri illustres), vjerojatno je utjecalo na Stankovićevo glavno djelo, na što upućuje i njegov naslov: “Biografia degli uomini distinti dell\'Istria“ – “Životopisi znamenitih ljudi iz Istre“.</p>')

    } else if (localStorage.getItem("jezik") == "eng") {

        $("#hr").css({
            "font-weight": "normal"
        })
        $("#eng").css({
            "font-weight": "bold"
        })

        $("#it").css({
            "font-weight": "normal"
        })

        $(".izmijena").html('<p>PETAR STANKOVIĆ – PIETRO STANCOVICH (Barbana, 24 febbraio 1771 - Barbana, 12 settembre 1852) canonico e storiografo, iniziò il suo lungo percorso scolastico e di studi a Rovigno e grazie alla grande dedizione per l’attività intellettuale e al desiderio di imparare, ebbe durante la sua vita enormi successi nel campo delle scienze. Il suo costante e attivo impegno nello studio lo arricchiva intellettualmente, portandolo anche ad essere membro di numerose accademie.</p><p> Analizzando l\'opera omnia di Stancovich, ovvero i ventitre scritti pubblicati, i suoi manoscritti e i titoli dei libri da lui collezionati con passione per la sua biblioteca, notiamo la varietà degli interessi personali di questo personaggio. I volumi da lui donati alla città di Rovigno illustrano le tendenze della vita sociale, economica e culturale dell\'epoca e i cambiamenti del territorio. Stancovich ci ha pertanto offerto un autentico tesoro per le ricerche e lo studio del patrimonio librario che non ha solo un mero valore storico e pratico, ma anche un significato artistico quale parte dell\'identità civica e nazionale. L’uomo Pietro Stancovich ci ricorda che non esiste vita proficua senza abnegazione.</p>')

    } else {
        $("#hr").css({
            "font-weight": "normal",
            "opacity": "0.5"
        })
        $("#eng").css({
            "font-weight": "normal"
        })

        $("#it").css({
            "font-weight": "bold",
            "opacity": "1"
        })
        $(".naznas").text("Pagina iniziale")
        $(".nazdjela").text("Opere")
        $(".naztis").text("Rarità di stampa")
        $(".naz16").text("Rarità del secolo XVI")
        $(".nazzbi").text("Stancoviciana")
        $(".opisslike").text("Petar Stanković, autoritratto, 1792.")
        $(".nazsta").text("Pietro Stancovich")
        $(".str").text("p.")
        $(".list").text("foglio")
        $(".listova").text("fogli")
          $(".muz").text("Museo della Città di Rovinj – Rovigno")



        $(".naslov").text("RARITÀ DEL SECOLO XVI")
        $(".izmijena").html('<p> Lo Stancovich si era procurato le opere di quasi tutti gli scrittori classici romani e cristiani più significativi. Il libro più antico della sua biblioteca è l\'opera di Eusebio del 1501. L\'opera di Plinio “Naturalis historia“ è uno dei primi compendi eniciclopedici in Europa, frequentemente pubblicato e tradotto. Anche lo stesso Stancovich si è occupato di scienze naturali, soprattutto di zoologia e botanica per cui aveva acquistato libri di questo contenuto.</p><p> L\' opera di Svetonio “La vita degli imperatori“, che contiene la biografia di 12 imperatori romani, ha influito notevolmente su molte biografie scritte dal Medio evo in poi. Stancovich ha tratto lo spunto per il suo capolavoro probabilmente da quest\'opera e dall\'altro libro di Svetonio “Viri illustres“ (Uomini illustri), come lo indica il titolo stesso: “Biografia degli uomini distinti dell\'Istria“.</p>')
    }
}
/*
$(document).scroll(function() {
    var st = $(this).scrollTop();
    $("#header").css({
        "background-position-y": (-st / 20)
    })
    $("#headerc").css({
        "top": (-st / 5),
        "bottom": (st / 5)
    })
});

window.onscroll = function() {
    myFunction()
};

var navbar = document.getElementById("navbar");
var sidebar = document.getElementById("sidenav");
var sticky = navbar.offsetTop;

function myFunction() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add("sticky")
        sidebar.classList.add("sticky")
    } else {
        navbar.classList.remove("sticky");
        sidebar.classList.remove("sticky");

    }
}
*/
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
    dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var dropdownContent = this.nextElementSibling;
        if (dropdownContent.style.display === "block") {
            dropdownContent.style.display = "none";
        } else {
            dropdownContent.style.display = "block";
        }
    });
}




$(document).mouseup(function(e) {
    var container = $(".slikica");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $("#glass").hide();
    }
});